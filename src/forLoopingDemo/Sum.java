package forLoopingDemo;

import java.util.Scanner;

public class Sum {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("Enter the start point");
        int start= sc1.nextInt();
        System.out.println("Enter end point");
        int end=sc1.nextInt();
        double sum=0.0;
        for(int a=start;a<=end;a++)
        {
            if(a%2==0) {
                sum = sum + a;
            }

        }
        System.out.println(sum);



    }
}
