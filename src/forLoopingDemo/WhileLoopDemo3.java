package forLoopingDemo;
import java.util.Scanner;
public class WhileLoopDemo3 {
    public static void main(String[] args) {
        Scanner sc1 = new Scanner(System.in);
        boolean status = true;
        while (status) {
            System.out.println("1:Addition");
            System.out.println("2:Subtraction");
            System.out.println("3:Exit");
            int choice = sc1.nextInt();

            if (choice == 1) {
                System.out.println("Enter the num");
                int num1 = sc1.nextInt();
                System.out.println("Enter the num");
                int num2 = sc1.nextInt();
                System.out.println(num1 + num2);
            } else if (choice == 2) {
                System.out.println("Enter the num");
                int num1 = sc1.nextInt();
                System.out.println("Enter the num");
                int num2 = sc1.nextInt();
                System.out.println(num1 - num2);
            } else {
                status = false;
            }
        }
    }
}


