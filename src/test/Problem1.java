package test;
import java.util.Scanner;
public class Problem1 {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("Enter the element");
        int element=sc1.nextInt();
        int[] arr={10,11,12,13,14,15};
        boolean found=false;
        for (int a=0;a< arr.length;a++){
            if(arr[a]==element) {
                System.out.println("Position of an element is " + a );

                found = true;
            }
        }
        if(!found)
        {
            System.out.println("Element not found");
        }

    }
}
