package abstraction;

public class SavingsAccount implements Account{
    double accountBalance;
    public SavingsAccount(double accountBalance){
        this.accountBalance=accountBalance;
        System.out.println("Savings account created");

    }

    @Override
    public void deposit(double amt) {
        accountBalance+=amt;
        System.out.println(amt +"Rs Created to your account");

    }

    @Override
    public void withdraw(double amt) {
        if (amt<=accountBalance){
            accountBalance-=amt;
            System.out.println(amt+"Rs debited from your account");
        }else {
            System.out.println("Insufficient Balance");
        }

    }

    @Override
    public void checkBalance() {
        System.out.println("Active Balance"+accountBalance);

    }
}
