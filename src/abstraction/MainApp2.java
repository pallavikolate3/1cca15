package abstraction;

import java.util.Scanner;

public class MainApp2 {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("Select account type");
        System.out.println("1:Savings\n2:Loan");
        int accType= sc1.nextInt();
        System.out.println("Enter account balance");
        double balance= sc1.nextDouble();

        AccountFactory factory=new AccountFactory();
        Account accRef=factory.createAccount(accType,balance);
        boolean status=true;
        while(status){
            System.out.println("Slect mode of transaction");
            System.out.println("1:Deposit\n2:Withdraw\n3:Check balance\n4:Exit");
            int choice=sc1.nextInt();
            if (choice==1)
            {
                System.out.println("Enter amount");
                double amt= sc1.nextDouble();
                accRef.deposit(amt);
            } else if (choice==2) {
                System.out.println("Enter Amount");
                double amt= sc1.nextDouble();
                accRef.withdraw(amt);

            } else if (choice==3) {
                accRef.checkBalance();

            }else {
                status=false;
            }
        }
    }
}
