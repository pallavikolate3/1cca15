package sadp;

public abstract class AbstractFactory {
    abstract Shape getShape(String shapeType);
}
