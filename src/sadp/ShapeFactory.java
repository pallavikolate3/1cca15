package sadp;

public class ShapeFactory extends AbstractFactory {
    @Override
     Shape getShape(String shapeType) {
        if (shapeType.equalsIgnoreCase("Reactangle")){
            return new Reactangle();
        }
        else if (shapeType.equalsIgnoreCase("Square")) {
            return new RoundedSquare();
        }
        return null;
    }
}
