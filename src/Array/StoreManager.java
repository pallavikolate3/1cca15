package Array;

public class StoreManager {
    static String[] products={"TV","PROJECTOR","MOBILES"};
    static double[] cost={15000,25000,30000};
    static int[] stock={10,40,50};

    void calculateBill(int choice,int qty)
    {
        boolean found=false;
        for(int a=0;a<products.length;a++)
        {
            if(choice==a && qty<=stock[a])
            {
                double total=qty*cost[a];
                stock[a]-=qty;
                System.out.println("TOTAL BILL AMOUNT IS"+total);
                found=true;
            }
        }
        if (!found)
        {
            System.out.println("products not available or stock not ready");
        }
    }
}
