package Array;

import java.util.Scanner;

public class arrayDemo4 {
    public static void main(String[] args) {
        Scanner sc1 = new Scanner(System.in);
        System.out.println("Enter total number of subject");
        int size = sc1.nextInt();
        double[] marks = new double[size];
        System.out.println("Enter subject marks");
        double total = 0.0;
        double percent=0.0;
        for (int a = 0; a < size; a++) {
            marks[a] = sc1.nextDouble();

            total += marks[a];
            percent=total/size;
        }
        System.out.println(total);
        System.out.println(percent);
    }
}
