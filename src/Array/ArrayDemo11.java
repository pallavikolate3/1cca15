package Array;

public class ArrayDemo11 {
    public static void main(String[] args) {
        int[][] data;
        data=new int[2][2];
        data[0][0]=100;
        data[0][1]=200;
        data[1][0]=300;
        data[1][1]=400;

        for (int[] datum : data) {
            for (int b = 0; b < datum.length; b++) {
                System.out.print(datum[b] + "\t");
            }
            System.out.println();
        }

    }
}
