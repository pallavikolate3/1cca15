package Array;

public class ArrayDemo14 {
    public static void main(String[] args) {
        int[][][] data;
        data=new int[2][2][2];
        data[0][0][0]=100;
        data[0][0][1]=200;
        data[0][1][0]=300;
        data[0][1][1]=400;

        data[1][0][0]=500;
        data[1][0][1]=600;
        data[1][1][0]=700;
        data[1][1][1]=800;

        for (int a=0;a< data.length;a++){
            for (int b=0;b< data[a].length;b++){
                for (int c=0;c< data[a][b].length;c++){
                    System.out.print(data[a][b][c]+"\t");
                }
            }
            System.out.println();
        }
    }
}
