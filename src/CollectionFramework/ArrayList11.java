package CollectionFramework;

import java.util.Scanner;

public class ArrayList11 {
    public static void main(String[] args) {
        Scanner sc1 = new Scanner(System.in);
        BookOperations b = new BookOperations();

        boolean status = true;
        while (true) {
            System.out.println("Select mode to perform operation");
            System.out.println("1:add the object");
            System.out.println("2:Display the list");
            System.out.println("3:Remove the element");
            System.out.println("4:Exit");

            int choice = sc1.nextInt();
            switch (choice) {
                case 1:
                    System.out.println("Enter the book id");
                    int id = sc1.nextInt();

                    System.out.println("Enter the book name");
                    String name = sc1.next();

                    System.out.println("Enter book price");
                    double price = sc1.nextDouble();

                    b.addBook(id,name,price);
                    break;

                case 2:
                    b.displayBook();
                    break;

                case 3:
                    System.out.println("Enter book id");
                    int bookId= sc1.nextInt();
                    b.deleteBook(bookId);
                    break;

                case 4:
                    status=false;
                    break;
            }
        }
    }
}
